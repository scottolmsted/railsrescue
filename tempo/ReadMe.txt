Responsive HTML5 website template for startups

Theme name:
=======================================================================
Tempo

Theme version:
=======================================================================
v1.4

Release Date:
=======================================================================
04 Feb 2015

Author: 
=======================================================================
Xiaoying Riley at 3rd Wave Media

Contact:
=======================================================================
Web: http://themes.3rdwavemedia.com/
Email: hello@3rdwavemedia.com
Twitter: 3rdwave_themes
