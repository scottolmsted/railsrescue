
<h3 class="subtitle">Scott Olmsted</h3>

<img alt='Scott Olmsted' class='thumb' src='/assets/images/people/scott.png'>
Scott M. Olmsted, PhD is a software engineer with more than 25 years of experience.
Since 1992 he has been an independent consultant and contractor working for a wide 
variety of clients, including Qualcomm, AT&T, General Atomics, L3 Communications,
Rokenbok, and many smaller firms.

He created his first website in 1997, and has now focused exclusively on web development 
using Ruby on Rails. He manages much of the work done by Rails Rescue.


<h3 class="subtitle">Barbara Walker</h3>

<img alt='Barbara Walker' class='thumb' src='/assets/images/people/btw.jpg'>
Barbara Walker has been helping clients with SEO and other aspects of their websites
for more than ten years. Ever helpful (and cheerful!), Barbara is the go-to person
for gaining visibility for your web application.

<br>
