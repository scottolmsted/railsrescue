<span class="anchor" id="code_evaluation"></span>
<h3 class="subtitle">Code Evaluation: $399</h3>

Do you have an existing web application built on Ruby on Rails? Do developers have trouble adding new features? Are you unable to move it from one host to another? Is the code quality less than stellar? Does it need upgrading? Does it contain security risks? What kind of effort is needed to begin work on adding new features?

If you have questions like these, we can answer them. In a Code Evaluation we will spend up to six hours evaluating your existing Rails project, with the goal to answer the questions you have. We will attempt to make the code run in a development environment, exercise the tests, if present, evaluate the general quality of the code, look specifically at any problems you are having, plus examine the database, the deployment technology, and the hosting environment.

When complete we'll tell you roughly how much "technical debt" is present in the code (how much has been done "quick and dirty" and how much adheres to standards of good coding practice), and what kind of effort is needed for us to add new features, fix bugs, and bring the quality of the code to a higher level.

A Code Evaluation can be purchased for <b>$399</b> via PayPal. We will need access to the code and database. The code is usually kept on Github.com or Bitbucket.com; you can add us as a contiributor. If hosted at Heroku, you can add us there as a collaborator. We will tell you exactly how to do that.


<span class="anchor" id="upgrades"></span>
<h3 class="subtitle">Upgrades</h3>

Is your Ruby on Rails application built on Rails 4.1 or later, with the latest update installed? If not, then you are risking incursion by those who do nothing but look for security holes on the web. New security risks are patched regularly, but you have to get the updates installed for them to work.

The later versions of Ruby and Rails are also quite a bit faster than older versions. We have seen significant "snappiness" added to a site simply by upgrading Ruby to a recent version.

And if you want new features or other significant work done on your Rails, upgrading may not just save development time, but might be required, as new components needed may be incompatible with the old.

It is impossible to give a fixed price for upgrades, as there may be significant code changes required. After a Code Evaluation we can perform upgrades for $75/hour.


<span class="anchor" id="design"></span>
<h3 class="subtitle">Application Design</h3>

Are you starting from scratch to create a web application, perhaps an MVP, or conversion of a business process? Then the most important step is to design the application in enough detail that developers know exactly what they should do to bring it to life.

The application design phase typically comprises up to one third of the total effort. Application design services are billed at $80/hour.


<span class="anchor" id="development"></span>
<h3 class="subtitle">Development </h3>

Development means creating the software code that tells your web application what to look like and how to behave. Software development is mostly a <i>craft</i>, a combination of knowledge, experience, and judgment combined to create something attractive and useful. The spectrum of skill and ability among developers is very wide. In our rescue projects we have seen code from everywhere on the spectrum, though bad code is, unfortunately, more common than good code.

When we develop your web application, we want to leave you with code and documentation that will be easy for another developer to pick up and continue with. That's our goal. Quite often we come back to out own code after some time has passed and must learn anew how it works, so we create the kind of code we will be happy to see again.

Software development is available at $90/hour.
