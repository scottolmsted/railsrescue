<span class="anchor" id="price"></span>
<h3 class="subtitle">Price</h3>

This is usually the first question clients ask, so let's start with it.

We may be able to give you a fixed bid under one of two conditions: 1) you have a written scope of work and a design sufficiently detailed to allow us to estimate the hours needed to complete the work, or 2) you hire us on an hourly basis to create these things (you could also get bids from other developers in addition to our bid).

Otherwise, we charge hourly rates for application design and application implementation. Application design is done by a senior developer, implementation by both senior and junior developers.


<span class="anchor" id="confidentiality"></span>
<h3 class="subtitle">Confidentiality</h3>

We used to gladly sign non-disclosure agreements (NDA's) when asked. But in our long experience, no NDA that covered mere ideas has ever preceded a project for which we were hired. NDA's that protect mere ideas are useless, and might be used as a platform to sue us without cause.

Ideas are plentiful, it's follow-through and action that are in short supply. That said, we promise not to steal your ideas, NDA or not. We've heard a lot of ideas from clients and potential clients and have never been tempted to use one.

But if you have ideas embodied in code, proprietary business processes, or similar intellectual property to protect, then we are happy to sign an NDA.

<span class="anchor" id="contract"></span>
<h3 class="subtitle">Contract</h3>

We have a short contract we prefer to use. Among other things it says that our relationship can be terminated by either party at any time and that you don't own the code until you pay for it. We may be able to use your contract if those two provisions are present.


<span class="anchor" id="payment"></span>
<h3 class="subtitle">Payment</h3>

One way we keep our rates lower than other agencies is by minimizing the time and effort spent collecting payment for our services.

For startups and many firms this means that we get a deposit before beginning work. If your project is small the deposit may also be small. We then work against the deposit, much like attorneys do, and ask for another deposit before the funds are exhausted.

At all times the remaining deposit is a credit to your account and can be refunded to you if not used to pay for our work. We do not work "until the funds are gone", we work according to your directions.

Our record of dealing fairly with clients is spotless; no client has ever said that we kept funds that were not ours to keep. We are happy to supply references.

If you are an established, profitable firm large enough to have an HR department, we would like to bill twice monthly, net 15, 1.5% per month late fee, but can be flexible if your accounting department has a different schedule.

We accept payments through PayPal for an extra 3%, which just covers their fee to us. PayPal will accept your credit card.

Unless you have a fantastic idea, a great team, and the money to market it, we will not be able to work for equity in your company.


<span class="anchor" id="started"></span>
<h3 class="subtitle">How do we get started?</h3>

For additions and changes to existing websites, we need the code! It is usually located on a repository such as GitHub.com or BitBucket.com. Sometimes it is found only on the web server for your site; if so, our first task will be to get a copy into a repository.

We also need a copy of the database. There are various ways to get this. And there may be other resouces we need to access to work on your project.

For both new and existing applications, we need direction on what you want done. For new "greenfield" projects the initial effort will be specifying and designing your site. For existing sites the initial effort will be to understand the organization of the existing code and to understand the changes you want. 


<span class="anchor" id="how_long"></span>
<h3 class="subtitle">How long will it take?</h3>

The short answer is "we don't know exactly". But that's not quite right; we can estimate any part of the project, or the whole project, at any time. All estimates we give are intervals, that is, we say "X will likely take between 6 and 10 hours". The smaller the task and the more similar it is to others we've done, the narrower the range of the interval. To estimate a larger task or project more accurately requires breaking it down into smaller tasks first: more accurate estimation takes more time, so we only want to do it if there is a requirement for it or substantial value to reducing cost uncertainty.

Usually we do something like this: you prioritize the features that you want, and we produce as many as possible within the given timeframe or budget, reviewing frequently to make sure that we are going in the direction you want. This is called "agile" development, and has been shown to be an effective way to handle a complex project with priorities that may change.


<span class="anchor" id="all_you_need"></span>
<h3 class="subtitle">"All you need to do is..."</h3>

Unless we are starting your project from scratch (a "greenfield" project), we will be working with your existing code. Quite often we hear "all you need to do is..." followed by some relatively simple-sounding changes or additions, and the expectation expressed that "it shouldn't take long".

We hate to rain on your parade, but quite often the changes desired will take longer than you think. What looks simple on a web page can require complex code. And testing. And rewriting of existing code. And other things.

Sometimes it depends on the quality of the existing code. Rather than go into details here, be assured that the list of possible code problems is very long, and we have seen most of them. Poor code quality usually requires extra work from us. So if you have existing code, be prepared for this.

