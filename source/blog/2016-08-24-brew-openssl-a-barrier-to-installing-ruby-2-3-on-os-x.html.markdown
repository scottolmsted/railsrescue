---
title: Brew, OpenSSL a barrier to installing Ruby 2.3 on OS X
date: 2016-08-24 23:27 UTC
author: scott
tags:
---

I went to install Ruby 2.3.1 on my Macbook Air today using rvm.

```
rvm install ruby-2.3.1
```

No can do:

```
Error running '__rvm_make -j 1',
showing last 15 lines of /Users/scott/.rvm/log/1472071369_ruby-2.3.1/make.log
                      ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ossl_ssl.c:18:35: note: expanded from macro 'numberof'
#define numberof(ary) (int)(sizeof(ary)/sizeof((ary)[0]))
                                  ^~~~~
ossl_ssl.c:2267:21: error: invalid application of 'sizeof' to an incomplete type 'const struct (anonymous struct at ossl_ssl.c:85:14) []'
    for (i = 0; i < numberof(ossl_ssl_method_tab); i++) {
                    ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ossl_ssl.c:18:35: note: expanded from macro 'numberof'
#define numberof(ary) (int)(sizeof(ary)/sizeof((ary)[0]))
                                  ^~~~~
6 warnings and 9 errors generated.
make[2]: *** [ossl_ssl.o] Error 1
make[1]: *** [ext/openssl/all] Error 2
make: *** [build-ext] Error 2
++ return 2
There has been an error while running make. Halting the installation.
```

Some research had me try this:

```
$ brew link --overwrite openssl --force
Warning: Refusing to link: openssl
Linking keg-only openssl means you may end up linking against the insecure,
deprecated system OpenSSL while using the headers from Homebrew's openssl.
Instead, pass the full include/library paths to your compiler e.g.:
  -I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib
```

Even after updating to the latest brew version of openssl, this same message appeared.

Following another lead I updated openssl with brew and symlimked that to ```/usr/local```. Same error message.

After more research and a few more fruitless tries, here is what worked for me. First I removed all brew versions of openssl:


```
$ brew list
$ ... openssl  openssl098 ...
$ brew uninstall --force openssl
$ brew uninstall --force openssl098
```

Then I followed the directions at [Installing .NET Core on Mac OS X](http://bytefish.de/blog/net_core_mac_os/) to install openssl into OS X directly. I got the latest stable version from [OpenSSL Downloads](https://www.openssl.org/source/): openssl-1.0.2h.tar.gz ([click here to download](https://www.openssl.org/source/openssl-1.0.2h.tar.gz)), unpacked the source:

```
$ cd ~/Downloads
$ tar xzf openssl-1.0.2h.tar.gz
$ cd openssl-1.0.2h
```

then compiled and installed OpenSSL to ```/usr/local/ssl/macos-x86_64``` (the last step takes some time).

```
$ ./Configure darwin64-x86_64-cc shared enable-ec_nistp_64_gcc_128 no-ssl2 no-ssl3 no-comp --openssldir=/usr/local/ssl/macos-x86_64
$ make depend
$ sudo make install
```

Last, I created symbolic links for the OpenSSL binaries and libraries in the /usr/local directory:

```
$ sudo ln -s /usr/local/ssl/macos-x86_64/bin/openssl /usr/local/bin/openssl
$ sudo ln -s /usr/local/ssl/macos-x86_64/lib/libssl.1.0.0.dylib /usr/local/lib/libssl.1.0.0.dylib
$ sudo ln -s /usr/local/ssl/macos-x86_64/lib/libcrypto.1.0.0.dylib /usr/local/lib/libcrypto.1.0.0.dylib
```

At this point we have liftoff:

```
$ rvm install ruby-2.3.1
Searching for binary rubies, this might take some time.
...
Install of ruby-2.3.1 - #complete 

```









