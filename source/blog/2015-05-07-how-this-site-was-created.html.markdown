---
title: How This Site Was Created
date: 2015-05-07 15:31 UTC
author: scott
tags: middleman, static sites
disqus_identifier: 2015-05-07-how-this-site-was-created
disqus_title: How This Site Was Created
disqus_url: http://railsrescue.com/blog/2015-05-07-how-this-site-was-created/
disqus_disable_mobile: true
---

The RailsRescue.com website was not created with Rails, but with Middleman because it is a static site. This makeover with Middleman was completed in May, 2015; the previous version was created with [webby](http://webby.rubyforge.org/), which is no longer supported.

The makeover was deemed necessary for two reasons. First, the previous site was starting to look rather dated, and second, the previous site was not responsive, which has become increasingly important.

Middleman was chosen because we have created several static sites with it in the last year and it has worked reasonably well. It is actively maintained and has excellent documentation.

This is not to say that we didn't encounter a glitch or two along the way. More on that in a moment.

We chose a Bootstrap theme at [Wrap Bootstrap](https://wrapbootstrap.com) called
 [Tempo](https://wrapbootstrap.com/theme/tempo-designed-for-startups-WB07364T3). It had just about every page wanted for this site, or a page that could easily be adapted.
 
In the Tempo theme each page is standalone HTML, so we cut these into a few partials (_head, _header, _footer, _javascript, _testimonial) and reduced `layout.haml` to this:

```html
!!!
/[if IE 8] <html lang="en" class="ie8">
/[if IE 9] <html lang="en" class="ie9">
/ [if !IE]><!
%html{:lang => "en"}
  / <![endif]
  = partial "head"
  %body.home-page
    .wrapper
      = partial "header"
      = yield
      = partial "footer"
      /= partial "modal"

    / Javascript
    = partial "javascript"
```

As you can see, we like Haml. But Haml gave us two problems when we added the blog module to Middleman.

The first problem occured when we followed the Middleman documentation ([here](https://middlemanapp.com/basics/layouts/#nested-layouts)) for the way to do nested layouts:

```html
<% wrap_layout :layout do %>
  <article>
    <%= yield %>
  </article>
<% end %>
```

This is Erb and translates to this in Haml:

```ruby
- wrap_layout :layout do
  %article
    = yield
```

But the result of this is that the pages rendered are completely empty.

What is needed is this:

```ruby
= wrap_layout :layout do
  %article
    = yield
```
The second problem we encountered was an interaction between Markdown, used to translate blog text into HTML and built into Middleman, and Haml. Initially a fenced code block such as this:

    ```
    def my_cool_method(message)
      puts message
    end
    ```

was rendered with extra indenting like this:

```ruby
def my_cool_method(message)
                      puts message
                    end
```
This apparently arises because Haml is sensitive to spaces. The solution is to process the output of the `yield` in the nested layout with a method to preserve spaces:

```ruby
.content
  = find_and_preserve(yield)
```
Now code in blog posts is rendered properly:

```ruby
def my_cool_method(message)
  puts message
end
```


We host the site at Amazon S3 and use the `s3deploy' gem to upload the build files. We used [PicMonkey]() to create circle photos for the testimonials. See [How To Create Circle Photos](http://uncommonlysocial.com/create-circle-photos-with-picmonkey/).

That's it!
