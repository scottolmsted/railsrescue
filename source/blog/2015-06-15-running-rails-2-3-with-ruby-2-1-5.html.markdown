---
title: Running Rails 2.3 with Ruby 2.1.5
date: 2015-06-15 15:20 UTC
author: scott
tags: rails
---

Since 2010 we have been adding features to and maintaining a client's site that is built on Rails 2.3. Currently the site is using the LTS version of 2.3.18 and so is, we hope, secure. The site is hosted at RailsPlayground, with a MySQL database and the app running in a virtual machine.

Until recently the version of Ruby used was 1.8.7, Ruby Enterprise Edition. Because it is no longer supported, and because we thought there would be significant speed improvements with a recent version of Ruby, the site was upgraded to 2.1.5.

A good starting point is [Have a Rails 2 app? You can run it on the newest Ruby!](http://blog.lucascaton.com.br/index.php/2014/02/28/have-a-rails-2-app-you-can-run-it-on-the-newest-ruby/). From there we added the gem 'iconv' and a slight modification of the monkey patch posted by MasterLambaster at [undefined method insert_record for Array](https://github.com/rails/rails/issues/11026). This patch is a demonstration of how to call a (supposedly) protected method by using 'send'.

We also added the patch [How to silence UTF-8 warnings on Rails 2.3 with Ruby 1.9](http://makandracards.com/makandra/14445-how-to-silence-utf-8-warnings-on-rails-2-3-with-ruby-1-9), code [here](https://gist.github.com/grosser/599b47bea7f3c5a044cb).

An annoyance was warnings the 'Digest::Digest is deprecated', so based on [Ruby - Digest::Digest is deprecated; Use Digest](http://stackoverflow.com/questions/21184960/ruby-digestdigest-is-deprecated-use-digest) and [Use OpenSSL::Digest instead of deprecated OpenSSL::Digest::Digest](https://github.com/fog/fog/pull/2473), we added this:

```
module ActiveSupport
  class MessageVerifier
    private
      def generate_digest(data)
        require 'openssl' unless defined?(OpenSSL)
        OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new(@digest), @secret, data)
      end
  end
end
```
These three patches were added at the end of `environment.rb`.

And the site is quite a bit more snappy now with Ruby 2.1.5.

