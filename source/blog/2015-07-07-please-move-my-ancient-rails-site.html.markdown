---
title: Please move my (ancient) Rails site
date: 2015-07-07 17:06 UTC
author: scott
tags: rails
---

"Can you move my website to a new server?", asked a woman calling from New York City.

"Why does it need moving?"

"The developer is--well, just difficult and doesn't want to deal with it anymore and I don't want to deal with him."

I've heard this numerous times now: "Help, my developer has disappeared!" In one case the developer left a half-finished project but no way to get at the code, so I tracked him down and told him (truthfully) that the client would be suing him for the thousands of dollars that he paid while getting nothing in return. He gave me the code.

I asked how long she'd had the site.

"14 years."

"But Rails is only ten years old."

"Well, all I know is I've had the site for 14 years." Apparently it was converted to Rails from something else.

"Where is the site hosted now?"

"On some server of his. He's given me a dump of the code and the database."

He had put this dump on DropBox and given her the link, so I downloaded all 250MB.

I expressed some optimism that if the code were in decent shape I might be able to get it running fairly quickly. I don't remember exactly what I said, but I never promise results. I suppose I can be too optimistic, though.

I looked at the code quickly while she waited on the phone. It was old, Rails 2.2 on the face of it. Later I saw this in `environment.rb`: `RAILS_GEM_VERSION = '1.2.2'`. I changed it to `2.2.2`, but I'm still not sure which is right.

The site is a CMS for a furniture rental company. It shows the hundreds of furniture and decorator items you can rent, but does not handle orders or other e-commerce. The images are not kept in the database, but in a directory under Rails root, so I'm not sure why the database so large, about 150MB. I still haven't looked in it.

There was no Gemfile, but in an email to her the developer said (obviously writing to whomever took over the project):

"it looks like i might have vendored the gems i needed but there might be a dependency on rmagick. Here is a list of all the gems i have installed on my server and i've starred the ones that i think might be necessary.""

```
*actionmailer (2.2.2, 1.3.3, 1.3.2)
*actionpack (2.2.2, 1.13.6, 1.13.3, 1.13.2)
*actionwebservice (1.2.6, 1.2.3, 1.2.2)
*activerecord (2.2.2, 1.15.6, 1.15.3, 1.15.2)
*activeresource (2.2.2)
*activesupport (2.2.2, 1.4.4, 1.4.2, 1.4.1)
builder (2.1.2)
*cgi_multipart_eof_fix (2.5.0, 2.1)
daemons (1.0.10, 1.0.7)
dolores-shacontest (2.2.2)
fastthread (1.0.1, 1.0)
gem_plugin (0.2.3, 0.2.2)
mongrel (1.1.5, 1.0.1)
mongrel_cluster (1.0.5, 0.2.1)
passenger (2.2.15, 2.0.5)
rack (0.4.0)
*rails (2.2.2, 1.2.3, 1.2.2)
rake (0.8.3, 0.7.3)
RedCloth (4.2.3)
*rmagick (1.15.7)
rubygems-update (1.3.1, 0.9.4)
sources (0.0.1)
unicorn (1.1.3)
```

I could see this was not going to be a walk in the park, but a few years ago I moved a Rails 1.2 site to a new host. And she really wanted to get away from the developer who wanted her project gone from his life. So I made what turned out to be a mistake.

I said something like "Why don't I spend up to two hours seeing how far I can get on this? If we're lucky it will fall together and that might cover it, if not, I'll give you a report on what I find as soon as I can." I really wanted to help her.

But people hear what they want to hear. And they latch on to the first number you give and remember nothing but that. So what she heard was that I "promised" to move her site in two hours of my time.

I spent a generous hour (meaning it was more than an hour) importing the database and trying to run the code using Ruby 2.1.5 (I do not want to go back to 1.8.7). I figured I could apply the patches I recently used on another client's Rails 2.3 [project](/blog/2015-06-15-running-rails-2-3-with-ruby-2-1-5/) and maybe even upgrade Rails to 2.3.

I got partway there. I don't know how the list of gems above could be complete; there were instances of `acts_as_tree` in most of the models and that gem must be added. And I didn't know yet if ImageMagick was really needed or whether the latest version would work with an old rmagick gem.

But I decided after an hour (a generous hour) that I should let her know where things stood and that it was definitely going to take more than two hours. I wrote a long, complete email that included details and questions it would be nice if her old developer answered. I explained again how problematic old code is, and some of the hurdles I had to get over. I didn't charge the time it took to write the email, probably 1/2 hour.

That's when I got hit with "You promised!" "You said you could do this in two hours!" I flashed back about 18 years to my son as a preteen and his favorite phrase: "You promised!"

When it became clear I wasn't going to move her site for a two hour charge, she tried other avenues. "But you charged me for an hour and I've got nothing!" (Actually not true, she has a lot better idea of what it will take).

Is this the way New Yorkers deal with each other?

Later she wrote and revealed that she had tried two other developers, one at GoDaddy and one independent and each had turned her down after a glance at the age of the code. I pointed out that my site makes it clear I charge for my time, as I had also made clear on the phone, and said "...the difference is that I didn’t give up immediately as the others did. You sounded pretty positive that you really wanted to get away from [the developer], so I got your site partly reconstructed in an hour. The rest will take more than another hour, but how long I can’t say."

I offered to consider doing the job on a no-risk fixed price basis if she would tell me what she would pay for that. Not surpisingly, I haven't heard back on that. I refunded her payment for one hour of my time, keeping one hour, for which I probably spent three. Sigh. When will I learn?

P.S. While writing this she called again, basically with the same harangue. I pressed her for the value to her of the move. The amount she gave would pay for about 2 1/2 hours of my time. I declined, as I know it will take more than that to get this old code working, plus I run the risk of no compensation if I can't make it go. I had to say "There's no point in continuing this, I'm hanging up now, goodbye" and end the call while she made threats to give me a bad review. That gives me an idea for a site--RateYourDeveloper.com. What do you think?
