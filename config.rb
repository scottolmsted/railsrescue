#require 'pry'

# Time.zone = "UTC"

# Blog settings

set :haml, :format => :html5

activate :blog do |blog|
  blog.prefix = "blog"  # Adds a prefix to all links, template references and source paths

  blog.permalink = "{year}-{month}-{day}-{title}.html"  # Matcher for blog source files
  blog.sources = "{year}-{month}-{day}-{title}.html"
  blog.taglink = "tags/{tag}.html"
  blog.layout = "blog_single"
  # blog.summary_separator = /(READMORE)/
  blog.summary_length = 250
  blog.year_link = "{year}.html"
  blog.month_link = "{year}/{month}.html"
  blog.day_link = "{year}/{month}/{day}.html"
  blog.default_extension = ".markdown"

  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  blog.paginate = true
  blog.per_page = 10
  blog.page_link = "page/{num}"
end

activate :directory_indexes
activate :protect_emails

activate :disqus do |d|
  # Disqus shotname, without '.disqus.com' on the end (default = nil)
  d.shortname = 'railsrescue'
end

# gem-based extensions
configure :development do
  activate :bh
  #activate :syntax
end

page "/feed.xml", layout: false

###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", layout: false
#
# With alternative layout
# page "/path/to/file.html", layout: :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# activate :livereload

# Methods defined in the helpers block are available in templates
helpers do
#   def some_helper
#     "Helping"
#   end

    def find_author(author_slug)
      author_slug = author_slug.downcase
      result = data.authors.select {|author| author.keys.first == author_slug }
      raise ArgumentError unless result.any?
      result.first
    end
end

#set :partials_dir, 'partials'

set :css_dir, 'assets/css'

set :js_dir, 'assets/js'

set :images_dir, 'assets/images'

# Markdown
set :markdown_engine, :redcarpet
set :markdown, :fenced_code_blocks => true, :smartypants => true

# Code highlighting
#activate :rouge_syntax

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end

# Copy .s3deploy.yml to /build
after_build do
  # note: config[:source] but config[:build_dir]
  system("cp #{config[:source] + '/.s3deploy.yml'} #{config[:build_dir] + '/.s3deploy.yml'}")
end

# Disable warnings for temple gem
# https://stackoverflow.com/questions/43934383/middleman-4-0-0-with-haml-template-engine-gives-error-when-starting-with-server
Haml::TempleEngine.disable_option_validator!
